#!/usr/bin/env python3
# encoding: utf-8

from help import Help

class Command(object):

    # Default class variable used for the help command
    _brief       = "== No brief available for this command =="
    _description = "== No description available for this command =="

    def __init__(self):

        # Support command separators for the current cmd
        if not hasattr(self, 'cmdSeparator'):
            self.cmdSeparator = []

        if not hasattr(self, 'cmdList'):
            self.cmdList = []

        # Creation of the cmdList for each components
        for obj in dir(self):

            if isinstance(getattr(self, obj), Command):
                self.cmdList.append(obj)

                # Adding the '.' command separator
                if self.cmdSeparator.count('.') == 0:
                    self.cmdSeparator.append('.')

            # Adding the '(' command separator
            if (obj == '_function'):
                if self.cmdSeparator.count('(') == 0:
                    self.cmdSeparator.append('(')

    def __call__(self, *paramsTuple):

        extractedParams = self.extractParams(paramsTuple)
        # Execute the command
        rc = self._function(*extractedParams)

        return rc

    def __repr__(self):
        return Help().print_help(self)

    def extractParams(self, paramsTuple):
        extractedParams = []

        # loop through the input parameter list
        for idx in range(len(paramsTuple)):
            if self._input.params[idx].symbolicVals is not None:
                extractedParams.append(self._input.params[idx].symbolicVals[paramsTuple[idx]])
            else:
                extractedParams.append(paramsTuple[idx])

        return extractedParams


class Module(Command):

    def __init__(self):
        super(Module, self).__init__();


class Params(object):

    def __init__(self, *params):
        # Creation of list of parameters
        self.params = params

        # Creation of dictionary of parameters
        self.paramsByName = {}
        for param in params:
            self.paramsByName[param.name] = param

    def __getitem__(self, idx):
        return self.params[idx]


class Input(object):

    def __init__(self, name, description, symbolicVals=None):
        self.name         = name
        self.description  = description
        self.symbolicVals = symbolicVals


class Output(object):

    def __init__(self, name, description):
        self.name         = name
        self.description  = description


class RetObject(object):

    def __init__(self):
        self.params = [w for w in dir(self) if not w.startswith('__')]
        super(RetObject, self).__init__();

