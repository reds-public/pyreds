#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Help(object):

    def alignement_space(self, length, lenght_max):
        space = ' '

        return space * (lenght_max-length)

    def function_params_help(self, params, initial_space):

        # Message parameters
        intentation = ' '*2
        titleSpcae  = initial_space + intentation
        paramSpace  = initial_space + intentation*2

        # Get the max length for the current parameters
        length_max = 0
        for param in params.params:
            cmd_length = len(param.name)
            if cmd_length > length_max:
                length_max = cmd_length

        for param in params.params:

            tmp_msg = '%(titleSpcae)s%(paramName)s: %(description)s' %\
                                    {'titleSpcae' :  titleSpcae,
                                     'paramName' : param.name,
                                     'description' : param.description}

            valuesMsg = ''
            if (hasattr('param', 'symbolicVals')) and (param.symbolicVals is not None):
                supportedValLengthMax = 0
                for supportedVal in param.symbolicVals.keys():
                    supportedValLength = len(str(supportedVal))
                    if supportedValLength > supportedValLengthMax:
                        supportedValLengthMax = supportedValLength

                for supportedVal in param.symbolicVals.keys():
                    valuesMsgTmp = '%(paramSpace)s%(supportedVal)s%(alignSpace)s, value: %(symbolicVals)s' %\
                            {'paramSpace'   : paramSpace,
                             'alignSpace'   : self.alignement_space(len(str(supportedVal)), supportedValLengthMax),
                             'supportedVal' : supportedVal,
                             'symbolicVals'  : str(param.symbolicVals[supportedVal])}

                    valuesMsg = '%s\n%s' % (valuesMsg, valuesMsgTmp)

            try:
                message = '%s\n%s%s' % (message, tmp_msg, valuesMsg)
            except NameError:
                message = '%s%s' % (tmp_msg, valuesMsg)

        return message

    def sub_cmd_help(self, command):
        message = "list of the available sub-commands\n"

        # Get the length of the longer sub-commands
        length_max = 0
        for sub_cmd in command.cmdList:
            cmd_length = len(sub_cmd)
            if cmd_length > length_max:
                length_max = cmd_length


        # build the Sub-command message
        for sub_cmd in command.cmdList:
            brief       = getattr(command, sub_cmd)._brief
            align_space = self.alignement_space(len(sub_cmd), length_max)
            sub_cmd_msg = '%(space)s%(subCmd)s%(subCmdAligSpace)s: %(subCmdBrief)s' % \
                                           {'space'           : (' '*2),
                                            'subCmd'          : sub_cmd,
                                            'subCmdAligSpace' : align_space,
                                            'subCmdBrief'     : brief}

            message = '%s%s\n' % (message, sub_cmd_msg)

        return message

    def function_help(self, command):

        title = 'Function parameters'

        initialSpace = ' '* 2
        intentation  = initialSpace*2

        inputParamTitle = "%sInput parameters" % initialSpace
        if hasattr(command, '_input'):
            inputParamMsg = self.function_params_help(command._input, initialSpace)
        else:
            inputParamMsg = "%sfunction has no 'input' parameters" % intentation

        outputParamTitle = "%sOutput parameters" % initialSpace
        if hasattr(command, '_output'):
            outputParamMsg = self.function_params_help(command._output, initialSpace)
        else:
            outputParamMsg = "%sfunction has no 'output' parameters" % intentation

        return """\
%(title)s
%(inputParamTitle)s
%(inputParamMsg)s
%(outputParamTitle)s
%(outputParamMsg)s""" % {'title'                : title,
                         'inputParamTitle'  : inputParamTitle,
                         'inputParamMsg'    : inputParamMsg,
                         'outputParamTitle' : outputParamTitle,
                         'outputParamMsg'   : outputParamMsg}

    def print_help(self, command):

        # Adding the command description
        help_msg = "%s\n\n" % (command._description)

        for cmdSeparator in command.cmdSeparator:

            if cmdSeparator == '.':
                # -- Sub-Command help message ----------------------------------
                help_msg = help_msg + self.sub_cmd_help(command) + '\n'

            if cmdSeparator == '(':
                # -- Function help message -------------------------------------
                 help_msg = help_msg + self.function_help(command) + '\n'

        return help_msg
