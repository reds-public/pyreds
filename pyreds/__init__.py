import os
import sys

sys.path.append(os.path.join(os.getcwd(), 'pyreds'))

from console import InteractiveConsole
from command import Command, Module, Params, Input, Output, RetObject
